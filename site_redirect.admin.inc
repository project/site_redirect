<?php

function site_redirect_configure_form($form_state) {

  $sites =  variable_get('site_redirect_sites', array());
	$key = 0;

	$form['sites'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Sites'),
	  '#description' => t('Please match country codes to sites. If the Transporter module is enabled on the destination site enable it to be used for the redirect so that pages can be matched automatically.'),
	  '#collapsible' => FALSE,
	  '#tree' => TRUE,
	);
	
	foreach ($sites as $site) { 

	    $form['sites'][$key]['country_code'] = array(
	      '#type' => 'textfield',
	      '#default_value' => $site['country_code'],
	      '#size' => 100,
	      '#maxlength' => 255,
	    );
	
	    $form['sites'][$key]['domain'] = array(
	      '#type' => 'textfield',
	      '#default_value' => $site['domain'],
	      '#size' => 100,
	      '#maxlength' => 255,
	    );
	
      $form['sites'][$key]['transporter'] = array(
        '#type' => 'checkbox',
        '#default_value' => $site['transporter'],
      );

      $key++;
	}
	
	for ($i = $key; $i < ($key + 5); $i++) { 

    $form['sites'][$i]['country_code'] = array(
      '#type' => 'textfield',
      '#default_value' => '',
      '#size' => 10,
      '#maxlength' => 10,
    );

    $form['sites'][$i]['domain'] = array(
      '#type' => 'textfield',
      '#default_value' => '',
      '#size' => 100,
      '#maxlength' => 255,
    );

    $form['sites'][$i]['transporter'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
    );
	} 
	
	$form['save'] = array(
	  '#type' => 'submit',
	  '#value' => t('Save'),
	);
		
  return $form;
}

function site_redirect_configure_form_validate($form, &$form_state) { 
  foreach ($form_state['values']['sites'] as $key => $site) {

		if (empty($site['country_code']) && !empty($site['domain'])) {
		  form_set_error("sites][$key][country_code", t('Country Code needs to be set.'));	
		}
		
		if (!empty($site['country_code']) && empty($site['domain'])) {
		  form_set_error("sites][$key][domain", t('Site URL needs to be set.'));	
		}
  }
}

function site_redirect_configure_form_submit($form, &$form_state) { 
  foreach ($form_state['values']['sites'] as $site) {
  	$key = $site['country_code'];
		$sites[$key] = $site;
  }

	variable_set('site_redirect_sites', $sites);
}

function theme_site_redirect_configure_form($form) {

	$header = array();
	$rows = array();

	$header = array(t('Country Code'), t('URL'), t('Transporter'));

  foreach (element_children($form['sites']) as $site_row) {
    $row = array();

	  foreach ($form['sites'][$site_row] as $site) {	 
	    $row[] = drupal_render($form['sites'][$site_row]['country_code']);
	    $row[] = drupal_render($form['sites'][$site_row]['domain']);
	    $row[] = drupal_render($form['sites'][$site_row]['transporter']);
	  } 
			     
		$rows[] = $row; 
  }

  $form['sites']['#value'] = theme('table', $header, $rows);

	$output = drupal_render($form);
  return $output;
}