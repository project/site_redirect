********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Site Redirect Module
Author: Robert Castelo <www.codepositive.com>
Drupal: 6.x
********************************************************************
DESCRIPTION:

Redirect user to another site based on a country code.

User Case
A .com site has bee closed down, all traffic to that site is 
re-directed to a .co.uk site

Redirect rules are set up so that: 

example.com/path_1 

goes to:

example.co.uk/site_redirect/path_1

This module then detects the country code of the user based on their 
IP address, and redirects the user to the site appropriate to their 
country, if a site exists.

For example if the user is based in Germany, and there is a German site, 
they will be redirected to it.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes. 

********************************************************************
CONFIGURATION:

Match country codes to sites:

/admin/settings/site_redirect/redirect

If you have the Transporter module enabled on these sites you can 
specify that the redirect uses Transporter to find a matching path.


********************************************************************
AUTHOR CONTACT

   
- Commission New Features:
   http://drupal.org/user/3555/contact
 



