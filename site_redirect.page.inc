<?php

/**
 * Process incoming request and redirect.
 */
function site_redirect_process($incoming_path = '') {
	global $db_url;
	$domain = '';
	
  $sites =  variable_get('site_redirect_sites', array());
	
  
	if (!is_array($db_url)) {
		// Module not configured in settings.php
		/**
		 * @todo set error message and don't proceed
		 */
	}
	
	$ip = ip_address();
  $country_code = ip2country_get_country($ip);

  // Match country code to site.
  if (isset($sites[$country_code])) {
	  $domain = $sites[$country_code]['domain'];
	  $transporter = isset($sites[$country_code]['transporter']) ? $sites[$country_code]['transporter'] : FALSE;
    $url = site_redirect_domain_url($domain, $incoming_path, $transporter);
  }
	else {
    $url = $incoming_path;
	} 

  drupal_goto($url);	
}

/**
 * Process incoming request and redirect.
 */
function site_redirect_domain_url($domain, $incoming_path, $transporter = FALSE) {
  if (empty($incoming_path)) {
  	return $domain;
  }

  if ($transporter) {
  	return $domain .'/transporter/'. $incoming_path;
  }

  return $domain .'/'. $incoming_path;
}

